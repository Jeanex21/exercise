package com.elavon.training.calculatorimpl;

public interface AwesomeCalculator {

	int getSum(int augend, int addend);

	double getDifference(double minuend, double subtrahend);

	double getProduct(double multiplicand, double multiplier);

	String getQuotientAndRemainder(double dividend, double divisor);

	double toCelsius(int fahrenheit);

	double toFahrenheit(int celsius);

	double toKilogram(double lbs);

	double toPound(double kg);

	boolean isPalindrome(String str);

	int getSum(int[] summands);

	double getProduct(double[] factors);

}
