package com.elavon.training.impl;

import java.io.*;

public class Calculator {

	public static InputStreamReader reader = new InputStreamReader(System.in);

	public static BufferedReader input = new BufferedReader(reader);

	public static int number1 = 0; 

	public static int number2 = 0; 

	public static void main(String[] args) throws IOException {

		enternumber();

	}

	public static void choices() throws IOException

	{

		do {

			System.out.println("Welcome to Java Sample Calculator!"); 

			System.out.println("Enter [1] for Addition");

			System.out.println("Enter [2] for Subtraction");

			System.out.println("Enter [3] for Multiplication");

			System.out.println("Enter [4] for Division");

			int useroption = Integer.parseInt(input.readLine());

			switch (useroption)

			{

			case 1:
				int answer1 = number1 + number2; // addition

				System.out.println("The answer is " + answer1);

				wantmore();

				return;

			case 2:
				int answer2 = number1 - number2; // subtraction

				System.out.println("The answer is " + answer2);

				wantmore();

				return;

			case 3:
				int answer3 = number1 * number2; // multiplication

				System.out.println("The answer is " + answer3);

				wantmore();

				return;

			case 4:
				int answer4 = number1 / number2; // division

				System.out.println("The answer is " + answer4);

				wantmore();

				return;

			default:

				return;

			}

		}
		
		while (true);

	}

	public static void wantmore() throws IOException

	{

		// option if you want to continue

		System.out.println("Do you want more? Press [y] for Yes and [n] for No");

		String option = input.readLine();

		switch (option) {

		case "y":
			enternumber();

			return;

		case "n":
			System.exit(0);

		default:
			return;

		}

	}

	public static void enternumber() throws IOException

	{

		System.out.print("Enter First number/s: "); // enter 1st number

		number1 = Integer.parseInt(input.readLine());

		System.out.print("Enter Second number/s: "); // enter 2nd number

		number2 = Integer.parseInt(input.readLine());

		choices();

	}

}
