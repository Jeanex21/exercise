
	public interface NetworkConnection {
		public NetworkConnection connect(String name);
		public boolean connectionStatus();
	}
